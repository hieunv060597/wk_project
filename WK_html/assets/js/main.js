(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
            back_to_top();
            header_stuck();
            moblie_bar();
            chosse_flag();
            slider_home();
            change_hot_deal_to_slide();
            slider_client();
        });             
    };
})(jQuery);



function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function moblie_bar() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}

function chosse_flag(){
    var langArray = [];
    $('.vodiapicker option').each(function(){
    var img = $(this).attr("data-thumbnail");
    var text = this.innerText;
    var value = $(this).val();
    var item = '<li><img src="'+ img +'" alt="" value="'+value+'"/></li>';
    langArray.push(item);
    })

    $('#a').html(langArray);

    //Set the button value to the first el of the array
    $('.btn-select').html(langArray[0]);
    $('.btn-select').attr('value', 'en');

    //change button stuff on click
    $('#a li').click(function(){
        var img = $(this).find('img').attr("src");
        var value = $(this).find('img').attr('value');
        var text = this.innerText;
        var item = '<li><img src="'+ img +'" alt="" /></li>';
        $('.btn-select').html(item);
        $('.btn-select').attr('value', value);
        $(".b").toggle();
    });

    $(".btn-select").click(function(){
            $(".b").toggle();
        });

         //check local storage for the lang
        var sessionLang = localStorage.getItem('lang');
        if (sessionLang){
        //find an item with value of sessionLang
        var langIndex = langArray.indexOf(sessionLang);
        $('.btn-select').html(langArray[langIndex]);
        $('.btn-select').attr('value', sessionLang);
        } else {
        var langIndex = langArray.indexOf('ch');
        $('.btn-select').html(langArray[langIndex]);
        //$('.btn-select').attr('value', 'en');
    }
}

function slider_home(){
    $('.banner-slide .owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots:false,
        autoplay:true,
        autoplayTimeout:6000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
        0:{
            items:1,
        },
        576:{
           items:1,
        },
        768:{
           items:1,
        },
        1024:{
            items:1,
        },
        1400:{
           items:1,          
        }
    }
  })
}

function change_hot_deal_to_slide(){
    var recent_free_self = $('.hot-deal .recent-free-self');
    var self_in = $('.hot-deal .recent-free-self .self-in');
    var $window = $(window);
    var windowsize = $window.width();
    

    if(windowsize <= 768){
        for(var i = 0; i<self_in.length; i++){
            self_in.removeClass('col-lg-4');
            self_in.addClass('item');
        }
        recent_free_self.removeClass('row');
        recent_free_self.addClass('owl-carousel owl-theme');
    }
    else{
        $(window).bind("resize",function(){
            if($(this).width() <= 768){
                
                for(var i = 0; i<self_in.length; i++){
                    self_in.removeClass('col-lg-4');
                    self_in.addClass('item');
                }
                recent_free_self.removeClass('row');
                recent_free_self.addClass('owl-carousel owl-theme');
                
            }
        })
    }

    $('#home-page .hot-deal .owl-carousel').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:true,
        autoplay:true,
        autoplayTimeout:6000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
        0:{
            items:1,
        },
        576:{
           items:2,
        },
        768:{
           items:2,
        },
        1024:{
            items: 3,
        },
        1400:{
           items:3,          
        }
    }
  })
}

function slider_client(){
    $('.client-feel .slide-client .owl-carousel').owlCarousel({
        loop:true,
        margin:25,
        nav:true,
        navText: ["<i class = 'fa fa-angle-left'></i>", "<i class = 'fa fa-angle-right'></i>"],
        dots:false,
        autoplay:true,
        autoplayTimeout:6000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
        0:{
            items:2, 
        },
        576:{
           items:2,
        },
        768:{
            items:3,
        },
        1024:{
            items:5,
        },

        1400:{
           items:5,
        }
     }
  })
}

function header_stuck(){
    var top_bar = document.querySelector('#header .top-bar');
    var header_main = document.querySelector('#header .header-main');
    var check = true;

    window.addEventListener('scroll', function(){
        if(window.pageYOffset > 260){
            if(check == true){
                top_bar.classList.add('off-top-bar');
                header_main.classList.add('stuck-header');
                check = false;
            }
        }
        else{
            if(check == false){
                top_bar.classList.remove('off-top-bar');
                header_main.classList.remove('stuck-header');
                check = true;
            }
        }
    })
}

